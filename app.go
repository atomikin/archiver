package main

import (
	"archive/zip"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"
)

func main() {
	var home string
	if runtime.GOOS == "windows" {
		home = os.Getenv("USERPROFILE")
	} else {
		home = os.Getenv("HOME")
	}
	config, err := LoadConfig(path.Join(home, ".archiver", "config.json"))
	if err != nil {
		log.Fatal(err)
	}
	semaphore := make(chan bool, runtime.NumCPU())
	out := make(chan *string)
	for _, target := range config.Targets {
		log.Printf("Started target %s", target.Path)
		go func(target Target) {
			semaphore <- true
			defer func() { <-semaphore }()
			res, err := target.DoZip()
			if err != nil {
				str := fmt.Sprintf("Failed to process target: %s; error: %s", target.Path, err)
				out <- &str
			} else {
				str := fmt.Sprintf("%s -> %s", target.Path, *res)
				out <- &str
			}
		}(target)
	}
	for count := 0; count < len(config.Targets); count++ {
		res := <-out
		if res != nil {
			log.Println(*res)
		} else {
			log.Printf("Processed: %s", *res)
		}
	}

}

func (self *Target) DoZip() (*string, error) {
	log.Printf("target %s", self.Path)
	if _, err := os.Stat(self.Storage); err == nil || prepareDest(self.Storage) {
		root, _ := filepath.Split(self.Path)

		outPath := path.Join(
			self.Storage, fmt.Sprintf("%s.zip",
				strconv.FormatInt(time.Now().UTC().UnixNano(), 10)),
		)
		out, err := os.Create(outPath)
		defer out.Close()
		if err != nil {
			return nil, err
		}
		writer := zip.NewWriter(out)
		re := regexp.MustCompile(self.Mask)
		err = filepath.Walk(self.Path, func(fPath string, i os.FileInfo, err error) error {
			if stat, _ := os.Stat(fPath); !stat.IsDir() {
				if _, name := filepath.Split(fPath); re.MatchString(name) {
					// if _, file := filepath.Split(path);
					archFile := fPath[len(root):]
					// there should be only forward slashes in the zip
					archFile = strings.Replace(archFile, "\\", "/", -1)

					// 0x800 - adds the unocode support to ZIP
					h := &zip.FileHeader{Name: archFile, Method: zip.Deflate, Flags: 0x800}
					file, err := writer.CreateHeader(h)
					if err != nil {
						return err
					}
					source, err := os.Open(fPath)
					if err != nil {
						return err
					}
					defer source.Close()
					_, err = io.Copy(file, source)
					if err != nil {
						return err
					}
				}
			}
			return nil
		})
		if err != nil {
			log.Fatal(err)
		}
		err = writer.Close()
		if err != nil {
			return nil, err
		}
		return &outPath, nil
	} else {
		return nil, errors.New("Illegal target")
	}
}

func prepareDest(dest string) bool {
	stat, err := os.Stat(dest)
	if err == nil && stat.IsDir() {
		return true
	} else if os.IsNotExist(err) {
		os.MkdirAll(dest, 0700)
		return true
	}
	return false
}
