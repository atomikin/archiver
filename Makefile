bin=bin

make-bin:
	mkdir -p ${bin}

build: make-bin app.go config.go
	go build -o ${bin}/archiver .

test: make-bin test.go
	cd src/
	go test
