package main

import (
	"encoding/json"
	"errors"
	"os"
	"path"
	"path/filepath"
)

const (
	DEFAULT_CONFIG = `{"targets": []}`
)

type Target struct {
	Path    string `json:"path"`
	Mask    string `json:"mask"`
	Storage string `json:"storage"`
}

type Config struct {
	Targets []Target `json:"targets"`
}

func LoadConfig(config_path string) (*Config, error) {
	var config Config
	stat, err := os.Stat(config_path)
	if err == nil && stat.IsDir() {
		return nil, errors.New("Config path is a folder")
	}
	if os.IsNotExist(err) {
		os.MkdirAll(path.Dir(config_path), 0700)
		file, err := os.Create(config_path)
		if err != nil {
			return nil, err
		}
		defer file.Close()
		file.WriteString(DEFAULT_CONFIG)
		return &config, nil
	}
	file, err := os.Open(config_path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	if err = decoder.Decode(&config); err != nil {
		return nil, err
	}
	for _, target := range config.Targets {
		root, err := filepath.Abs(target.Path)
		if err == nil {
			target.Path = root
		}
		storage, err := filepath.Abs(target.Storage)
		if err == nil {
			target.Storage = storage
		}
	}
	return &config, err
}
