package main

import (
	"log"
	"os"
	"path"
	"testing"
)

func TestLoading(t *testing.T) {
	log.Println("fdf")
	conf, err := LoadConfig("../src")
	if err == nil || conf != nil {
		t.Errorf("Found error %s", err)
	}
	conf_path := path.Join("..", ".archive", "c.json")
	if _, err := os.Stat(conf_path); err == nil {
		os.Remove(path.Dir(conf_path))
	}
	conf, err = LoadConfig(conf_path)
	if conf == nil {
		t.Error("Conf should not be nil")
	}
	if err != nil {
		t.Errorf("Error found: %s", err)
	}
	if l := len(conf.Targets); l != 0 {
		t.Error("JSON decode error")
	}
	f, err := os.Create(conf_path)
	if err != nil {
		panic(err)
	}
	f.WriteString(`{"targets": [{"path": "./", "mask": "*", "storage": "../"}]}`)
	f.Close()
	conf, err = LoadConfig(conf_path)
	if err != nil || conf == nil {
		t.Errorf("Failed to decode file: %s", err)
	}
	log.Printf("conf: ", conf.Targets)
	if len(conf.Targets) > 1 || conf.Targets[0].Mask != "*" || conf.Targets[0].Path != "./" || conf.Targets[0].Storage != "../" {
		t.Errorf("Parse error %s", conf)
	}
}
